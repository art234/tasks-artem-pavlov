﻿#include <iostream>
#include <string>
#include <cmath>

using namespace std;

struct Point { //Так как координаты x и y нe зависимы, принял решение весто класса использовать структуру
	double x = 0; //Это позволяет делать ввод точек без конструктора и напрямую обращаться к ним и изменять их
	double y = 0; //По этой же причине использовал функцию get_distance, а не метод 
};


double get_distance(const Point& A, const Point& B) {
	return sqrt(pow(A.x - B.x, 2) + pow(A.y - B.y, 2));
}


class Triangle {
public:

	Triangle(const Point& a = { 0, 0 }, const Point& b = { 0, 1 }, const Point& c = { 1, 0 }) { // Пользователь должен задавать сразу 3 точки
		A = a;
		B = b;
		C = c;
	}

	double get_perimeter() const {
		return get_distance(A, B) + get_distance(A, C) + get_distance(C, B); //используем уже имеющуюся функцию
	}

	double get_area() const {		//вычисление площади по формуле Герона
		double AB = get_distance(A, B);
		double AC = get_distance(A, C);
		double CB = get_distance(B, C);
		double p = (AB + AC + CB) * 0.5;
		return sqrt(p * (p - AB) * (p - AB) * (p - CB));
	}

private:
	Point A;
	Point B;
	Point C;
};

class Rectangle {

};

class Square: public Rectangle {

};

class Ellips {

};

class Circle: public Ellips {

};

int main() {
	Triangle triagle;
	return 0;
}